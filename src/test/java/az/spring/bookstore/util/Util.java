package az.spring.bookstore.util;

import az.spring.bookstore.entity.User;
import az.spring.bookstore.request.create.UserCreateRequest;
import az.spring.bookstore.response.create.UserCreateResponse;

public class Util {

    private Util(){

    }

    public static UserCreateRequest createRequest(){
        UserCreateRequest createRequest = new UserCreateRequest();
        createRequest.setFullname("test-fullname");
        createRequest.setUsername("test-username");
        createRequest.setPassword("test-password");
        return createRequest;
    }

    public static UserCreateResponse createResponse() {


        UserCreateResponse createResponse = new UserCreateResponse();
        createResponse.setFullname("test-fullname");
        createResponse.setUsername("test-username");
        createResponse.setPassword("test-password");
        return createResponse;
    }

    public static User user(){
        User user = new User();
        user.setFullname("test-fullname");
        user.setUsername("test-username");
        user.setPassword("test-password");
        return user;

    }
    public static User savedUser(){
        User savedUser = new User();
        savedUser.setFullname("test-fullname");
        savedUser.setUsername("test-username");
        savedUser.setPassword("test-password");
        return savedUser;

    }



}
