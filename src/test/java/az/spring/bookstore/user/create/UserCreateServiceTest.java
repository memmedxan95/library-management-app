package az.spring.bookstore.user.create;


import az.spring.bookstore.entity.User;
import az.spring.bookstore.mapper.UserMapper;
import az.spring.bookstore.repository.UserRepository;
import az.spring.bookstore.request.create.UserCreateRequest;
import az.spring.bookstore.response.create.UserCreateResponse;
import az.spring.bookstore.service.create.UserCreateService;
import az.spring.bookstore.util.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.mockito.Mockito.verifyNoMoreInteractions;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class UserCreateServiceTest {

    @Mock
    private UserMapper userMapper;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserCreateResponse userCreateResponse;

    @InjectMocks
    private UserCreateService userCreateService;


    @Test
    public void testUserCreateService_whenUserCreateRequset_shouldReturnUserCreateResponse(){

        UserCreateRequest createRequest = Util.createRequest();
        UserCreateResponse createResponse= Util.createResponse();
        User user = Util.user();
        User savedUser= Util.savedUser();


        Mockito.when(userMapper.createRequestToEntity(createRequest))
                .thenReturn(user);
        Mockito.when(userRepository.save(user)).thenReturn(savedUser);
        Mockito.when(userMapper.entityToResponseCreate(savedUser)).thenReturn(createResponse);

        UserCreateResponse checkData = userCreateService.createUser(createRequest);
        Assertions.assertEquals(checkData.getFullname(),createResponse.getFullname());
        Assertions.assertNotNull(checkData);

        Mockito.verify(userMapper).createRequestToEntity(createRequest);
        Mockito.verify(userRepository).save(user);
        Mockito.verify(userMapper).entityToResponseCreate(savedUser);
//        verifyNoMoreInteractions(userRepository);

    }



}
