package az.spring.bookstore.contraint.validation;

public class UserMessages {


    public static final String USER_NOT_FOUND = "User not found by id. ";
    public static final String USER_EXIST = "User exits, you can not create. ";
    public static final String USER_UNATHORIZE = "You dont have permission. ";
    public static final String USER_SUCCES_LOGIN = "Login is succesfully. ";
    public static final String USER_FAILED_LOGIN = "Login is failed. ";
}
