package az.spring.bookstore.contraint.validation;

public class BookMessages {

    public static final String BOOK_NOT_FOUND = "Book not found by id. ";
    public static final String BOOK_SUCCES_CREATED = "Book is succesfully created. ";


    public static final String BOOK_SUCCES_DELETED = "Book is succesfully deleted.";
    public static final String BOOK_SUCCES_UPDATED = "Book is succesfully updated.";
}
