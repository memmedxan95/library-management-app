package az.spring.bookstore.contraint.validation;

public class LibraryMessages {

    public static final String LIBRARY_NOT_FOUND = "Library not found by id. ";
    public static final String BOOK_ALREADY_EXITS = "Book already exits in library, add another one.";
}
