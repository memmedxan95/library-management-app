package az.spring.bookstore.contraint.validation;

import az.spring.bookstore.repository.UserRepository;
import az.spring.bookstore.request.login.UserLoginRequest;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;


@Component
@RequiredArgsConstructor
public class UserInfoValidator implements ConstraintValidator<UserInfo, UserLoginRequest> {

    private final UserRepository userRepository;

    @Override
    public boolean isValid(UserLoginRequest value, ConstraintValidatorContext context) {
        return StringUtils.hasText(value.getUsername()) ||  StringUtils.hasText(value.getPassword());
    }
}
