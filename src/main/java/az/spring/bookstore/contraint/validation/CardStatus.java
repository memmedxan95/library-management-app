package az.spring.bookstore.contraint.validation;

public enum CardStatus {

    ACTIVE ,
    DEACTIVE
}
