package az.spring.bookstore.contraint.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import jakarta.validation.constraints.NotNull;

import java.lang.annotation.*;

@Constraint(validatedBy = {UserInfoValidator.class})
@Target({ElementType.TYPE,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface UserInfo {


    String message() default "Firstname and lastname should be filled in ";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };




}
