package az.spring.bookstore.contraint.validation;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum UserStatus {


    @JsonProperty("admin")
    ADMIN,

    @JsonProperty("student")
    STUDENT
}
