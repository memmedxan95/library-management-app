package az.spring.bookstore.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity(name = "libraries")
@Getter
@Setter
public class Library {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "libraries_id", nullable = false)
    private Integer id;

    private String name;

    private String title;

    @Column(name = "fk_user_id")
    private Integer fkUserId;

    @Column(name = "fk_book_id")
    private Integer fkBookId;


    private String status;


    @PrePersist
    public void pre(){
        if (status==null){
            status = "C";
        }
    }
}
