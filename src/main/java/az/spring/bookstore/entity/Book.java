package az.spring.bookstore.entity;


import az.spring.bookstore.contraint.validation.BookStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.*;

@Getter
@Setter
@Entity(name = "books")
public class Book {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "books_id")
    private Integer id;

    @NotEmpty(message = "name must bu filled in")
    @Size(min = 2, max = 100, message = "lenght of title must be from 2 to 100.")
    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "author", nullable = false, length = 100)
    @NotEmpty(message = "author must bu filled in")
    @Size(min = 2, max = 100, message = "lenght of author must be from 2 to 100.")
    private String author;

    private String price;

    @Column(name = "fk_user_id")
    private Integer fkUserId;

    @Column(name = "fk_library_id")
    private Integer fkLibraryId;

    private String status;

    @PrePersist
    public void pre(){
        if (status==null){
            status = "A";
        }
    }


}
