package az.spring.bookstore.entity;


import az.spring.bookstore.contraint.validation.UserStatus;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.List;

import static jakarta.persistence.GenerationType.IDENTITY;


@Entity(name = "users")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "users_id")
    private Integer id;

//    @NotEmpty(message = "title must bu filled in")
//    @Size(min = 2, max = 100, message = "lenght of name must be from 2 to 100.")
//    @Column(name = "fullname",  length = 100)
    @Column(name = "fullname",unique = true)
    private String fullname;

    @Column(name = "username",unique = true)
    private String username;

    private String password;

    private String status;

    @Column(name = "fk_library_id")
    private Integer fkLibraryId;

    @Column(name = "fk_book_id")
    private Integer fkBookId;

    @PrePersist
    public void pre(){
        if (status==null){
            status = "Student";
        } else {
            status="Admin";
        }
    }



}
