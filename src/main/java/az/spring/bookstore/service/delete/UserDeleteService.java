package az.spring.bookstore.service.delete;

import az.spring.bookstore.entity.User;
import az.spring.bookstore.mapper.UserMapper;
import az.spring.bookstore.repository.UserRepository;
import az.spring.bookstore.request.delete.UserDeleteRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class UserDeleteService {

    private final UserMapper userMapper;


    private final UserRepository userRepository;

//    public Map<String, String> deleteUser(Integer id) {
//        Optional<User> user = userRepository.findById(id);
//        Map<String, String> response = new HashMap<>();
//        if (user.isPresent()) {
//            userRepository.deleteById(id);
//            response.put("message", "Deleted Successfully");
//        }
//        else {
//            response.put("message", "Employee id '" + id + "' does not exist");
//        }
//        return response;
//    }


     public void removeUser(UserDeleteRequest userDeleteRequest){
         userRepository.deleteById(userDeleteRequest.getId());
     }


}
