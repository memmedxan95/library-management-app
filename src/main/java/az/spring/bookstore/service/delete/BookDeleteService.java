package az.spring.bookstore.service.delete;


import az.spring.bookstore.contraint.validation.BookMessages;
import az.spring.bookstore.contraint.validation.UserMessages;
import az.spring.bookstore.entity.Book;
import az.spring.bookstore.entity.User;
import az.spring.bookstore.exception.UserNotFoundException;
import az.spring.bookstore.mapper.BookMapper;
import az.spring.bookstore.repository.BookRepository;
import az.spring.bookstore.repository.UserRepository;
import az.spring.bookstore.request.delete.BookDeleteRequest;
import lombok.RequiredArgsConstructor;
import org.hibernate.query.sql.internal.ParameterRecognizerImpl;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BookDeleteService {

    private final BookRepository bookRepository;
    private final UserRepository userRepository;

    public String deleteBook(BookDeleteRequest bookDeleteRequest) {
        User user = userRepository.findById(bookDeleteRequest.getFkUserId())
                .orElseThrow(() -> new UserNotFoundException(HttpStatus.NOT_FOUND.name(), UserMessages.USER_NOT_FOUND));
        if (user.getStatus().equals("Admin")) {
            bookRepository.deleteById(bookDeleteRequest.getId());
            user.setFkBookId(null);
            userRepository.save(user);
            return BookMessages.BOOK_SUCCES_DELETED;
        } else return UserMessages.USER_UNATHORIZE;
    }
}