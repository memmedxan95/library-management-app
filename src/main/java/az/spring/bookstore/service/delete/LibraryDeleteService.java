package az.spring.bookstore.service.delete;

import az.spring.bookstore.entity.Library;
import az.spring.bookstore.mapper.LibraryMapper;
import az.spring.bookstore.repository.LibraryRepository;
import az.spring.bookstore.request.delete.LibraryDeleteRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
@RequiredArgsConstructor
public class LibraryDeleteService {

    private final LibraryRepository libraryRepository;
    private final LibraryMapper libraryMapper;


    public void deleteLibrary(LibraryDeleteRequest libraryDeleteRequest){
        libraryRepository.findById(libraryDeleteRequest.getId())
                .ifPresent(library -> libraryRepository.deleteById(library.getId()));
    }

}
