package az.spring.bookstore.service.read;


import az.spring.bookstore.entity.User;
import az.spring.bookstore.exception.UserNotFoundException;
import az.spring.bookstore.mapper.UserMapper;
import az.spring.bookstore.repository.UserRepository;
import az.spring.bookstore.request.read.UserRaedRequest;
import az.spring.bookstore.response.read.UserReadResponse;
import az.spring.bookstore.wrapper.UserWrapper;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserReadService {

   private final UserRepository userRepository;
    UserMapper userMapper;

    public UserReadResponse read(UserRaedRequest raedRequest){

        User foundUser = userRepository.findById(raedRequest.getId()).orElseThrow(()-> new UserNotFoundException(HttpStatus.NOT_FOUND.name(), "User does not exits. "));
        UserReadResponse readResponse = userMapper.entityToResponseRead(foundUser);
        return readResponse;
    }

    public List<UserWrapper> getUserAllBooks(Integer id){
         return userRepository.findUserAllBooksCustom(id);
    }

}
