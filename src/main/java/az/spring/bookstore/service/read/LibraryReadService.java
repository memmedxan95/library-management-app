package az.spring.bookstore.service.read;


import az.spring.bookstore.entity.Library;
import az.spring.bookstore.entity.User;
import az.spring.bookstore.exception.LibraryNotFoundException;
import az.spring.bookstore.exception.UserNotFoundException;
import az.spring.bookstore.mapper.LibraryMapper;
import az.spring.bookstore.repository.LibraryRepository;
import az.spring.bookstore.repository.UserRepository;
import az.spring.bookstore.request.read.LibrarySpecificRaedRequest;
import az.spring.bookstore.response.read.LibraryReadResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LibraryReadService {

    private final LibraryRepository libraryRepository;
    private final UserRepository userRepository;
    private final LibraryMapper libraryMapper;

    public LibraryReadResponse getLibrary(LibrarySpecificRaedRequest librarySpecificRaedRequest){
        User user = userRepository.findById(librarySpecificRaedRequest.getFkUserId()).orElseThrow(()->new UserNotFoundException(HttpStatus.NOT_EXTENDED.name(), "User not found by id "));
        Library library = libraryRepository.findById(user.getFkLibraryId()).orElseThrow(()->new LibraryNotFoundException(HttpStatus.NOT_EXTENDED.name(), "Library not found by id "));
        return libraryMapper.entityToResponse(library);
    }

//    public List<LibraryReadResponse> readAllSpecificLibrary(LibrarySpecificRaedRequest request){
//        return libraryMapper.listEntityToListResponse(libraryRepository.findByFkUserId(request.getFkUserId()));
//    }
}
