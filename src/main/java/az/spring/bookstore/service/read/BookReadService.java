package az.spring.bookstore.service.read;


import az.spring.bookstore.entity.Book;
import az.spring.bookstore.mapper.BookMapper;
import az.spring.bookstore.repository.BookRepository;
import az.spring.bookstore.request.read.BookReadRequest;
import az.spring.bookstore.response.read.BookReadResponse;
import az.spring.bookstore.specification.BookSpecification;
import az.spring.bookstore.specification.SearchCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookReadService {

    private final BookRepository bookRepository;
    private final BookMapper bookMapper;

    public BookReadResponse getByName(BookReadRequest bookReadRequest){
        Book book = bookMapper.readRequestToEntity(bookReadRequest);
        Book bookRead = bookRepository.findById(book.getId()).get();
        BookReadResponse readResponse = bookMapper.entityToResponseRead(bookRead);
            return readResponse;
    }

    public List<Book> getAllBooksLibrary(Integer id){
       return (bookRepository.findAllByFkUserId(id));
    }


    public List<Book> findAllStudents(List<SearchCriteria> searchCriteriaList) {
        BookSpecification bookSpecification = new BookSpecification();
        searchCriteriaList.forEach(searchCriteria -> bookSpecification.add(searchCriteria));
        return bookRepository.findAll(bookSpecification);

    }


}
