package az.spring.bookstore.service.login;

import az.spring.bookstore.contraint.validation.LoginMessages;
import az.spring.bookstore.contraint.validation.UserMessages;
import az.spring.bookstore.entity.User;
import az.spring.bookstore.mapper.UserMapper;
import az.spring.bookstore.repository.UserRepository;
import az.spring.bookstore.request.create.UserCreateRequest;
import az.spring.bookstore.request.login.UserLoginRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;



@Service
@RequiredArgsConstructor
public class UserLoginService {

    private final UserRepository userRepository;

    private final UserMapper userMapper;



    public String login(UserLoginRequest userLoginRequest) {
        if (userRepository.findUserWithUsernameAndPassword(userLoginRequest.getUsername(), userLoginRequest.getPassword()).isEmpty()){
            return UserMessages.USER_SUCCES_LOGIN;
        } else return UserMessages.USER_FAILED_LOGIN;

}


}
