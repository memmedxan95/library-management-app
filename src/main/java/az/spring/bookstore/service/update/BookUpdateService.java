package az.spring.bookstore.service.update;

import az.spring.bookstore.contraint.validation.BookMessages;
import az.spring.bookstore.contraint.validation.UserMessages;
import az.spring.bookstore.entity.Book;
import az.spring.bookstore.entity.User;
import az.spring.bookstore.exception.BookNotFoundException;
import az.spring.bookstore.exception.UserNotFoundException;
import az.spring.bookstore.mapper.BookMapper;
import az.spring.bookstore.repository.BookRepository;
import az.spring.bookstore.repository.UserRepository;
import az.spring.bookstore.request.update.BookUpdateRequest;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static org.hibernate.sql.ast.SqlTreeCreationLogger.LOGGER;


@Service
@RequiredArgsConstructor
public class BookUpdateService {
    @PersistenceContext
    private EntityManager entityManager;

    private final BookRepository bookRepository;
    private final BookMapper bookMapper;
    private final UserRepository userRepository;

    public String updateBook(BookUpdateRequest bookUpdateRequest) {
        if (bookRepository.findById(bookUpdateRequest.getId()).isEmpty()) {
            throw new BookNotFoundException(HttpStatus.NOT_FOUND.name(), BookMessages.BOOK_NOT_FOUND);
        }
        User user = userRepository.findById(bookUpdateRequest.getFkUserId())
                .orElseThrow(() -> new UserNotFoundException(HttpStatus.NOT_FOUND.name(), UserMessages.USER_NOT_FOUND));
        if (user.getStatus().equals("Admin")) {
            Book bookUpdated = bookMapper.updateRequestToEntity(bookUpdateRequest);
            LOGGER.info(String.valueOf(bookUpdateRequest.getName()));
            LOGGER.info(String.valueOf(bookUpdateRequest.getAuthor()));
            bookUpdated.setId(bookUpdateRequest.getId());
            bookUpdated.setFkUserId(bookUpdateRequest.getFkUserId());
            bookRepository.save(bookUpdated);
            user.setFkBookId(bookUpdateRequest.getFkUserId());
            userRepository.save(user);
            return BookMessages.BOOK_SUCCES_UPDATED;
        } else return UserMessages.USER_UNATHORIZE;
    }
}