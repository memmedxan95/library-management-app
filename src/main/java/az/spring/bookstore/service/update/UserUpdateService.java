package az.spring.bookstore.service.update;


import az.spring.bookstore.entity.User;
import az.spring.bookstore.mapper.UserMapper;
import az.spring.bookstore.repository.UserRepository;
import az.spring.bookstore.request.update.UserUpdateRequest;
import az.spring.bookstore.response.update.UserUpdateResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserUpdateService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserUpdateResponse updateUser(UserUpdateRequest userUpdateRequest) {

       User user = userMapper.updateRequestToEntity(userUpdateRequest);

        Optional<User> userObj = Optional.ofNullable(userRepository.findByUsername(user.getUsername()));
        if (userObj.isPresent()) {
            User userData = userObj.get();
            if (user.getFullname() != null) {
                userData.setFullname(user.getFullname());
            }
            if (user.getUsername() != null) {
                userData.setUsername(user.getUsername());
            }
            if (user.getPassword() != null) {
                userData.setPassword(user.getPassword());
            }
            if (user.getStatus() != null) {
                userData.setStatus(user.getStatus());
            }
            userData.setFkBookId(userObj.get().getFkBookId());
            userData.setFkLibraryId(userObj.get().getFkLibraryId());
            userData.setId(user.getId());
             userRepository.save(userData);
            UserUpdateResponse userUpdateResponse = userMapper.entityToResponseUpdate(userData);
            return userUpdateResponse;
        }
        else {
            User savedUser = userRepository.save(user);
            return userMapper.entityToResponseUpdate(savedUser);
        }
    }


}
