package az.spring.bookstore.service.add;


import az.spring.bookstore.contraint.validation.BookMessages;
import az.spring.bookstore.contraint.validation.LibraryMessages;
import az.spring.bookstore.contraint.validation.UserMessages;
import az.spring.bookstore.entity.Book;
import az.spring.bookstore.entity.Library;
import az.spring.bookstore.entity.User;
import az.spring.bookstore.exception.BookAlreadyExitsException;
import az.spring.bookstore.exception.BookNotFoundException;
import az.spring.bookstore.exception.LibraryNotFoundException;
import az.spring.bookstore.exception.UserNotFoundException;
import az.spring.bookstore.mapper.LibraryMapper;
import az.spring.bookstore.repository.BookRepository;
import az.spring.bookstore.repository.LibraryRepository;
import az.spring.bookstore.repository.UserRepository;
import az.spring.bookstore.request.add.BookAddRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LibraryAddService {

    private final LibraryRepository libraryRepository;
    private final LibraryMapper libraryMapper;
    private final UserRepository userRepository;
    private final BookRepository bookRepository;

    public void addBook(BookAddRequest bookAddRequest) {
        User user = userRepository.findById(bookAddRequest.getFkUserId())
                .orElseThrow(() -> new UserNotFoundException(HttpStatus.NOT_FOUND.name(), UserMessages.USER_NOT_FOUND));
        user.setFkBookId(bookAddRequest.getId());
        userRepository.save(user);
        Library library = libraryRepository.findById(user.getFkLibraryId())
                .orElseThrow(() -> new LibraryNotFoundException(HttpStatus.NOT_FOUND.name(), LibraryMessages.LIBRARY_NOT_FOUND));
        if (checkBookExiting(bookAddRequest.getId(), user)) {
            throw new BookAlreadyExitsException(HttpStatus.BAD_REQUEST.name(), LibraryMessages.BOOK_ALREADY_EXITS);
        }
        library.setFkBookId(bookAddRequest.getId());
        libraryRepository.save(library);
        Book book = bookRepository.findById(bookAddRequest.getId())
                .orElseThrow(() -> new BookNotFoundException(HttpStatus.NOT_FOUND.name(), BookMessages.BOOK_NOT_FOUND));
        book.setFkUserId(bookAddRequest.getFkUserId());
        book.setFkLibraryId(book.getFkLibraryId());
        bookRepository.save(book);

    }


    public boolean checkBookExiting(Integer id, User user) {
        Library library = libraryRepository.findById(user.getFkLibraryId())
                .orElseThrow(() -> new LibraryNotFoundException(HttpStatus.NOT_FOUND.name(), LibraryMessages.LIBRARY_NOT_FOUND));
        return library.getFkBookId().equals(user.getFkLibraryId());
    }


}
