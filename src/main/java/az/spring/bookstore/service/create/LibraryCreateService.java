package az.spring.bookstore.service.create;

import az.spring.bookstore.contraint.validation.LibraryMessages;
import az.spring.bookstore.contraint.validation.UserMessages;
import az.spring.bookstore.entity.Library;
import az.spring.bookstore.entity.User;
import az.spring.bookstore.exception.LibraryExitsException;
import az.spring.bookstore.exception.UserNotFoundException;
import az.spring.bookstore.mapper.LibraryMapper;
import az.spring.bookstore.repository.LibraryRepository;
import az.spring.bookstore.repository.UserRepository;
import az.spring.bookstore.request.create.LibraryCreateRequest;
import az.spring.bookstore.request.read.LibrarySpecificRaedRequest;
import az.spring.bookstore.response.create.LibraryCreateResponse;
import az.spring.bookstore.response.read.LibraryReadResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class LibraryCreateService {

    private final LibraryRepository libraryRepository;
    private final UserRepository userRepository;
    private final LibraryMapper libraryMapper;

    public LibraryReadResponse create(LibraryCreateRequest libraryCreateRequest){
        User user = userRepository.findById(libraryCreateRequest.getFkUserId())
                .orElseThrow(()-> new UserNotFoundException(HttpStatus.NOT_FOUND.name(), UserMessages.USER_NOT_FOUND));

        if (Objects.nonNull(user)){
            Library library = libraryRepository
                    .findByTitleOrFkUserIdAndStatusIn(libraryCreateRequest.getName(),libraryCreateRequest.getFkUserId(), List.of("C", "A"));
        if(Objects.nonNull(library)){
            throw new LibraryExitsException(HttpStatus.NOT_FOUND.name(), LibraryMessages.BOOK_ALREADY_EXITS);
        } else {
            Library savedLibrary = libraryMapper.createRequestToEntity(libraryCreateRequest);
            libraryRepository.save(savedLibrary);
            user.setFkLibraryId(savedLibrary.getId());
            userRepository.save(user);
            return libraryMapper.entityToResponse(libraryRepository.save(savedLibrary));
        }
        }
        throw new UserNotFoundException(HttpStatus.NOT_FOUND.name(), UserMessages.USER_NOT_FOUND);
    }
}
