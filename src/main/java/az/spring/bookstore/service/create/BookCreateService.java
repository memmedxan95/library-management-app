package az.spring.bookstore.service.create;


import az.spring.bookstore.contraint.validation.BookMessages;
import az.spring.bookstore.contraint.validation.UserMessages;
import az.spring.bookstore.entity.Book;
import az.spring.bookstore.entity.User;
import az.spring.bookstore.exception.UserNotFoundException;
import az.spring.bookstore.mapper.BookMapper;
import az.spring.bookstore.repository.BookRepository;
import az.spring.bookstore.repository.UserRepository;
import az.spring.bookstore.request.create.BookCreateRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BookCreateService {

    private final BookRepository bookRepository;
    private final UserRepository userRepository;
    private final BookMapper bookMapper;

    public String createBook(BookCreateRequest bookCreateRequest) {
        User user = userRepository.findById(bookCreateRequest.getFkUserId())
                .orElseThrow(() -> new UserNotFoundException(HttpStatus.NOT_FOUND.name(), UserMessages.USER_NOT_FOUND));
        if (user.getStatus().equals("Admin")) {
            Book book = bookMapper.createRequestToEntity(bookCreateRequest);
            bookRepository.save(book);
            user.setFkBookId(bookCreateRequest.getId());
            userRepository.save(user);
            return BookMessages.BOOK_SUCCES_CREATED;
        } else return UserMessages.USER_UNATHORIZE;
    }
}