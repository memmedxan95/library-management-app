package az.spring.bookstore.service.create;


import az.spring.bookstore.contraint.validation.UserMessages;
import az.spring.bookstore.entity.User;
import az.spring.bookstore.exception.UserExitsException;
import az.spring.bookstore.mapper.UserMapper;
import az.spring.bookstore.repository.UserRepository;
import az.spring.bookstore.request.create.UserCreateRequest;
import az.spring.bookstore.response.create.UserCreateResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserCreateService {

    private final UserMapper userMapper;


    private final UserRepository userRepository;
    private UserCreateResponse userCreateResponse;

    @Transactional
    public UserCreateResponse createUser(UserCreateRequest userCreateRequest) {
        if (userRepository.findById(userCreateRequest.getId()).isPresent()) {
            throw new UserExitsException(HttpStatus.BAD_REQUEST.name(), UserMessages.USER_EXIST);
        } else {
            User user = userMapper.createRequestToEntity(userCreateRequest);
            User savedUser = userRepository.save(user);
            userCreateResponse = userMapper.entityToResponseCreate(savedUser);
        }
        return userCreateResponse;
    }
}