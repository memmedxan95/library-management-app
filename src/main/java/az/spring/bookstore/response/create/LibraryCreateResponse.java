package az.spring.bookstore.response.create;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LibraryCreateResponse {


    private Long id;
    private String title;
    private String name;
    private Integer fkUserId;
    private Integer fkBookId;
    private String status;
}