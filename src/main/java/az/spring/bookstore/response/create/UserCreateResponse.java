package az.spring.bookstore.response.create;

import az.spring.bookstore.contraint.validation.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateResponse {

    private Integer id;
    private String fullname;
    private String username;
    private String password;
    private String status;


}
