package az.spring.bookstore.response.read;

import az.spring.bookstore.contraint.validation.BookStatus;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookReadResponse {

    private Integer id;
    private String name;
    private String author;
    private String price;
    private Integer fkUserId;
}
