package az.spring.bookstore.response.read;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserReadResponse {

    Integer id;
    String name;
    String username;
    String password;
}
