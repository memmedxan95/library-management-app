package az.spring.bookstore.response.update;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookUpdateResponse {

//    private Integer id;
    private String name;
    private String author;
    private String price;
}
