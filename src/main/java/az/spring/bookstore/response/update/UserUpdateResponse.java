package az.spring.bookstore.response.update;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserUpdateResponse {

    String fullname;
    String username;
    String password;

}
