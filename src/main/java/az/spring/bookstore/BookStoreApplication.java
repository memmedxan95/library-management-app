package az.spring.bookstore;

import az.spring.bookstore.entity.Book;
import az.spring.bookstore.repository.BookRepository;
import az.spring.bookstore.service.create.BookCreateService;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class BookStoreApplication   {

	private final BookCreateService bookCreateService;
	private final BookRepository bookRepository;

	public static void main(String[] args) {
		SpringApplication.run(BookStoreApplication.class, args);
	}

//	@Override
//	public void run(String... args) throws Exception {
//		Book book = Book.
//
//	}
}
