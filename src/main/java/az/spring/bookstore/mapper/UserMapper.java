package az.spring.bookstore.mapper;


import az.spring.bookstore.entity.User;
import az.spring.bookstore.request.create.UserCreateRequest;
import az.spring.bookstore.request.delete.UserDeleteRequest;
import az.spring.bookstore.request.update.UserUpdateRequest;
import az.spring.bookstore.response.create.UserCreateResponse;
import az.spring.bookstore.response.read.UserReadResponse;
import az.spring.bookstore.response.update.UserUpdateResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface UserMapper {


    @Mapping( target="fullname", source="fullname" )
    @Mapping(target="status", source="status")
    User createRequestToEntity (UserCreateRequest userCreateRequest);
    User deleteRequestToEntity(UserDeleteRequest userDeleteRequest);

    UserCreateResponse entityToResponseCreate(User user);

    UserReadResponse entityToResponseRead(User user);

    @Mapping( target="fullname", source="fullname" )
    UserUpdateResponse entityToResponseUpdate(User user);

    @Mapping( target="fullname", source="fullname" )
    User updateRequestToEntity(UserUpdateRequest userUpdateRequest);



}
