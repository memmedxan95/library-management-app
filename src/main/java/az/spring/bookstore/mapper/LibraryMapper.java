package az.spring.bookstore.mapper;


import az.spring.bookstore.entity.Library;
import az.spring.bookstore.request.create.LibraryCreateRequest;
import az.spring.bookstore.response.read.LibraryReadResponse;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface LibraryMapper {


    Library createRequestToEntity(LibraryCreateRequest libraryCreateRequest);

    LibraryReadResponse entityToResponse(Library library);


List<LibraryReadResponse> listEntityToListResponse(List<Library> libraries);
}
