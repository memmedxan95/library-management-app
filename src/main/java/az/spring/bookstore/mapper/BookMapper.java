package az.spring.bookstore.mapper;

import az.spring.bookstore.entity.Book;
import az.spring.bookstore.request.create.BookCreateRequest;
import az.spring.bookstore.request.delete.BookDeleteRequest;
import az.spring.bookstore.request.read.BookReadRequest;
import az.spring.bookstore.request.update.BookUpdateRequest;
import az.spring.bookstore.response.create.BookCreateResponse;
import az.spring.bookstore.response.read.BookReadResponse;
import az.spring.bookstore.response.update.BookUpdateResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface BookMapper {

    @Mapping(target = "id", ignore = true)
    Book createRequestToEntity(BookCreateRequest bookCreateRequest);
    @Mapping(target = "id", ignore = true)
    Book readRequestToEntity(BookReadRequest bookReadRequest);
    @Mapping(target = "id", ignore = true)
    Book deleteRequestToEntity(BookDeleteRequest BookDeleteRequest);

    BookCreateResponse entityToResponseCreate(Book Book);

    BookReadResponse entityToResponseRead(Book Book);

    BookUpdateResponse entityToResponseUpdate(Book Book);

    @Mapping(target = "id", ignore = true)
    Book updateRequestToEntity(BookUpdateRequest bookUpdateRequest);

    List<BookReadResponse> entityListToResponseList(List<Book> bookList);
}
