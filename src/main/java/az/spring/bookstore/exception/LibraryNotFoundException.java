package az.spring.bookstore.exception;

public class LibraryNotFoundException extends RuntimeException{

    public LibraryNotFoundException (String code , String message){
        super(message);
    }
}
