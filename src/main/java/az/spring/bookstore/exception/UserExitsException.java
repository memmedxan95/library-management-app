package az.spring.bookstore.exception;

public class UserExitsException extends RuntimeException{

    public UserExitsException (String code , String message){
        super(message);
    }
}
