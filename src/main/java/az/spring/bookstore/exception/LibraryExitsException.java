package az.spring.bookstore.exception;

public class LibraryExitsException extends RuntimeException {

    public LibraryExitsException (String code , String message){
        super(message);
    }
}
