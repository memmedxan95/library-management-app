package az.spring.bookstore.exception;

public class BadRequestException extends RuntimeException {
    public BadRequestException(String code, String message) {
        super(message);
    }
}