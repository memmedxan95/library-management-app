package az.spring.bookstore.exception;

public class BookAlreadyExitsException extends RuntimeException {
    public BookAlreadyExitsException(String code, String message){
        super(message);
    }
}
