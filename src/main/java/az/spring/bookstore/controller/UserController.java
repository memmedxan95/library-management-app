package az.spring.bookstore.controller;

import az.spring.bookstore.contraint.validation.group.CreateAction;
import az.spring.bookstore.request.create.UserCreateRequest;
import az.spring.bookstore.request.delete.UserDeleteRequest;
import az.spring.bookstore.request.login.UserLoginRequest;
import az.spring.bookstore.request.update.UserUpdateRequest;
import az.spring.bookstore.response.create.UserCreateResponse;
import az.spring.bookstore.response.update.UserUpdateResponse;
import az.spring.bookstore.service.create.UserCreateService;
import az.spring.bookstore.service.delete.UserDeleteService;
import az.spring.bookstore.service.login.UserLoginService;
import az.spring.bookstore.service.read.UserReadService;
import az.spring.bookstore.service.update.UserUpdateService;
import az.spring.bookstore.wrapper.UserWrapper;
import jakarta.validation.groups.Default;
import lombok.RequiredArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserCreateService userCreateService;
    private final UserLoginService userLoginService;
    private final UserDeleteService userDeleteService;
    private final UserUpdateService userUpdateService;
    private final UserReadService userReadService;


        @GetMapping("/login")
    public ResponseEntity<String> login(@RequestBody @Validated({Default.class, CreateAction.class}) UserLoginRequest userLoginRequest) {
      return ResponseEntity.ok(userLoginService.login(userLoginRequest));
    }

    @PostMapping("/registration")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<UserCreateResponse> userRegistration(@RequestBody UserCreateRequest userCreateRequest){
     return ResponseEntity.status(HttpStatus.OK).body(userCreateService.createUser(userCreateRequest));
    }

    @DeleteMapping("/delete")
    public void deleteUser(@RequestBody UserDeleteRequest userDeleteRequest){
        userDeleteService.removeUser(userDeleteRequest);
    }

    @PutMapping("/update")
    public ResponseEntity<UserUpdateResponse> updateUser(@RequestBody UserUpdateRequest userUpdateRequest){
        return ResponseEntity.ok(userUpdateService.updateUser(userUpdateRequest));
    }

    @GetMapping("/userallbooks/{id}")
    public ResponseEntity<List<UserWrapper>> getAllUserBooks(@PathVariable Integer id){
            return ResponseEntity.ok().body(userReadService.getUserAllBooks(id));
    }





//    @PostMapping("/user/register")
//    public @ResponseBody User registerUser(@RequestBody User user) {
//        return userCreateService.saveUser(user);
//    }
//
//    @PutMapping("/user/{id}")
//    public @ResponseBody User UpdateUser(@RequestBody User user, @PathVariable Integer id) {
//        return userCreateService.updateUser(user, id);
//    }
//
//    @DeleteMapping("/user/{id}")
//    public @ResponseBody Map<String, String> deleteUser(@PathVariable Integer id) {
//        return userCreateService.deleteUser(id);
//    }
//
//    @GetMapping("/users")
//    public @ResponseBody List<User> showAllUsers() {
//        return userCreateService.getAllUsers();
//    }
//
//    @GetMapping("/user/{id}")
//    public @ResponseBody User getUser(@PathVariable Integer id) {
//        return userCreateService.getUser(id);
//    }





}
