package az.spring.bookstore.controller;


import az.spring.bookstore.entity.Book;
import az.spring.bookstore.repository.BookRepository;
import az.spring.bookstore.request.add.BookAddRequest;
import az.spring.bookstore.request.create.LibraryCreateRequest;
import az.spring.bookstore.request.delete.LibraryDeleteRequest;
import az.spring.bookstore.request.read.LibrarySpecificRaedRequest;
import az.spring.bookstore.response.read.BookReadResponse;
import az.spring.bookstore.response.read.LibraryReadResponse;
import az.spring.bookstore.service.add.LibraryAddService;
import az.spring.bookstore.service.create.LibraryCreateService;
import az.spring.bookstore.service.delete.LibraryDeleteService;
import az.spring.bookstore.service.read.BookReadService;
import az.spring.bookstore.service.read.LibraryReadService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/libraries/")
public class LibraryController {

    private final LibraryCreateService libraryCreateService;
    private final LibraryDeleteService libraryDeleteService;
    private final LibraryReadService libraryReadService;
    private final LibraryAddService libraryAddServiceService;
    private final BookRepository bookRepository;
    private final BookReadService bookReadService;

    @PostMapping("create")
    public ResponseEntity<LibraryReadResponse> create (@RequestBody @Valid LibraryCreateRequest libraryCreateRequest){
        return ResponseEntity.status(HttpStatus.CREATED).body(libraryCreateService.create(libraryCreateRequest));
    }

//    @PostMapping("read")
//    public ResponseEntity<List<LibraryReadResponse>> readAll(@RequestBody @Valid LibrarySpecificRaedRequest libraryCreateRequest){
//        return ResponseEntity.status(HttpStatus.OK).body(libraryReadService.readAllSpecificLibrary(libraryCreateRequest));
//    }

    @DeleteMapping("delete")
    public void delete(@RequestBody @Valid LibraryDeleteRequest libraryDeleteRequest){
        libraryDeleteService.deleteLibrary(libraryDeleteRequest);
    }

    @PostMapping("read")
    public ResponseEntity<LibraryReadResponse> read(@RequestBody @Valid LibrarySpecificRaedRequest librarySpecificRaedRequest){
        return ResponseEntity.status(HttpStatus.OK).body(libraryReadService.getLibrary(librarySpecificRaedRequest));
    }

    @PostMapping("add")
    public void addBook(@RequestBody @Valid BookAddRequest bookAddRequest){
         libraryAddServiceService.addBook(bookAddRequest);
    }

    @GetMapping ("userlibrary")
    public ResponseEntity<List<Book>> getAllBooksInLibrary(@RequestBody LibrarySpecificRaedRequest librarySpecificRaedRequest){
      return  ResponseEntity.ok().body(bookReadService.getAllBooksLibrary(librarySpecificRaedRequest.getFkUserId()))  ;
    }

}
