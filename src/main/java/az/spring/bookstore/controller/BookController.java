package az.spring.bookstore.controller;


import az.spring.bookstore.entity.Book;
import az.spring.bookstore.request.create.BookCreateRequest;
import az.spring.bookstore.request.delete.BookDeleteRequest;
import az.spring.bookstore.request.read.BookReadRequest;
import az.spring.bookstore.request.update.BookUpdateRequest;
import az.spring.bookstore.response.read.BookReadResponse;
import az.spring.bookstore.service.create.BookCreateService;
import az.spring.bookstore.service.delete.BookDeleteService;
import az.spring.bookstore.service.read.BookReadService;
import az.spring.bookstore.service.update.BookUpdateService;
import az.spring.bookstore.specification.SearchCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/books")
public class BookController {

    private final BookCreateService bookCreateService;
    private final BookReadService bookReadService;
    private final BookUpdateService bookUpdateService;
    private final BookDeleteService bookDeleteService;

    @PostMapping("/create")
    @ResponseStatus(CREATED)
    public ResponseEntity<String> createBook(@RequestBody BookCreateRequest bookCreateRequest){
      return ResponseEntity.ok(bookCreateService.createBook(bookCreateRequest));
    }


    @GetMapping("/get")
    public ResponseEntity<BookReadResponse> getBookByName(BookReadRequest bookReadRequest){
        return ResponseEntity.ok(bookReadService.getByName(bookReadRequest));
    }

    @PutMapping("/update")
    public ResponseEntity<String> updateBook(@RequestBody BookUpdateRequest bookUpdateRequest){
        return ResponseEntity.ok().body(bookUpdateService.updateBook(bookUpdateRequest));
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> deleteBook(@RequestBody BookDeleteRequest bookDeleteRequest){
        return ResponseEntity.ok().body(bookDeleteService.deleteBook(bookDeleteRequest));
    }

    @PostMapping("/specificSearch")
    public ResponseEntity<List<Book>> findAllStudents(@RequestBody List<SearchCriteria> searchCriteriaList) {
        return ResponseEntity.ok(bookReadService.findAllStudents(searchCriteriaList));
    }
}
