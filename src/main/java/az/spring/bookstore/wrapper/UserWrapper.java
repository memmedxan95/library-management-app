package az.spring.bookstore.wrapper;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserWrapper {

    private Integer id;
    private String fullname;
    private String username;
    private String password;

    private String author;
    private String price;

//    private String title;

}
