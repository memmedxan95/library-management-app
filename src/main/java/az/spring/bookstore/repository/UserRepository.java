package az.spring.bookstore.repository;

import az.spring.bookstore.entity.User;
import az.spring.bookstore.request.delete.UserDeleteRequest;
import az.spring.bookstore.wrapper.UserWrapper;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Integer> {


    @Query("SELECT u FROM users u WHERE u.username =:username AND u.password =:password")
    List<User> findUserWithUsernameAndPassword(String username, String password);

    User findByUsername(String name);


    @Query("select new az.spring.bookstore.wrapper.UserWrapper(a.id, a.fullname, a.username, a.password, " +
            "b.author, b.price ) from users a join books b on a.id=b.fkUserId where a.id=?1")
    List<UserWrapper> findUserAllBooksCustom(Integer id);






}
