package az.spring.bookstore.repository;

import az.spring.bookstore.entity.Library;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LibraryRepository extends JpaRepository<Library, Integer> {


    Library findByTitleOrFkUserIdAndStatusIn(String name, Integer fkUserId, List<String> status);

    List<Library> findByFkUserId(Integer fkuserid);

}
