package az.spring.bookstore.repository;

import az.spring.bookstore.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Integer> , JpaSpecificationExecutor<Book> {


    Book findByName(String name);


    @Query("SELECT u FROM books u WHERE u.fkUserId =:id")
    List<Book> findAllByFkUserId(Integer id);

}
