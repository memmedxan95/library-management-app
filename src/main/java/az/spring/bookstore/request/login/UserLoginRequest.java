package az.spring.bookstore.request.login;


import az.spring.bookstore.contraint.validation.UserInfo;
import az.spring.bookstore.contraint.validation.group.CreateAction;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@UserInfo(groups = CreateAction.class)
public class UserLoginRequest {

//    @NotNull
    private String username;
//    @NotNull
    private String password;
}
