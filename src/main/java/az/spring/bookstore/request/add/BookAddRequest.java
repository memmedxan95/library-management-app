package az.spring.bookstore.request.add;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookAddRequest {

    private Integer id;
    private String name ;
    private String author;
    private String price;
    private Integer fkUserId;

}
