package az.spring.bookstore.request.update;


import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookUpdateRequest {

    private Integer id;
    private String name;
    private String author;
    private String price;
    private Integer fkUserId;

}
