package az.spring.bookstore.request.create;


import az.spring.bookstore.contraint.validation.UserInfo;
import az.spring.bookstore.contraint.validation.UserStatus;
import jakarta.persistence.Column;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.mapstruct.Mapping;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateRequest {

    private Integer id;
    private String fullname;
    private String username;
    private String password;
    private Integer fkLibraryId;
    private Integer fkBookId;
    private String status;


}
