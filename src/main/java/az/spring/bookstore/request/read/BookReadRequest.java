package az.spring.bookstore.request.read;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookReadRequest {

//    private Integer id;
    private String name;
    private String author;
    private String price;

}
